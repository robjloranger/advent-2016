/*
This program takes a csv file from stdin
and reads the directions one word at a time.

A word should contain a L or R followed by the
number of blocks to travel before the next turn.

Tracks our coordinates in a city grid, then
returns the shortest distance to the end point.

2016 rob j loranger
*/

package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
)

type point struct {
	x float64
	y float64
}

func main() {
	// init coordinates
	// coordinates are cartesian
	// x represents east and west, pos & neg int
	// y represents north and south, pos & neg int
	p := point{0.0, 0.0}

	// locations holds all unique coordinates
	// ad the number of times we were there
	locations := make(map[point]int)

	// bool to stop tracking locations after
	// first double visit
	tracking := true

	// heading is the positions current heading
	// 0 = N, 1 = E, 2 = S, 3 = W
	heading := 0

	// get input from csv file
	input := bufio.NewScanner(os.Stdin)
	// split input on words
	input.Split(bufio.ScanWords)

	// scan over words
	for input.Scan() {
		word := input.Text() // grab word
		stop := len(word)    // grab length
		// if word ends with , reduce stop
		// to skip , for distance int
		if word[stop-1] == ',' {
			stop--
		}
		turn := rune(word[0])                               // get direction to turn toward
		blocks, err := strconv.ParseFloat(word[1:stop], 64) // distance to travel along headin
		if err != nil {
			log.Fatal(fmt.Errorf("error converting blocks string to an int: %v", err))
		}

		// rotate our positions heading
		if turn == 'L' {
			heading--
			if heading < 0 {
				heading += 4
			}
		} else {
			heading++
			if heading > 3 {
				heading -= 4
			}
		}

		// move along axis
		for blocks > 0 {
			if heading == 0 {
				p.y++
				blocks--
			} else if heading == 2 {
				p.y--
				blocks--
			} else if heading == 1 {
				p.x++
				blocks--
			} else {
				p.x--
				blocks--
			}
			// while we have not found a point twice
			if tracking {
				if locations[p] == 1 {
					tracking = false
					fmt.Println("Secret headquarters is: ", math.Abs(p.y)+math.Abs(p.x))
				} else {
					locations[p]++
				}
			}
		}
	}

	// calculate and return shortest distance
	fmt.Println("We are a total of ", math.Abs(p.y)+math.Abs(p.x), " blocks away.")
}
