package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"strconv"
	"time"
)

func main() {
	// track time for fun
	start := time.Now()
	// the door id we need the password for
	doorid := "uqwqemis"
	password := [8]string{}
	var idx, ccount int
	// loop until the password is 8 characters in length
	for ccount < 8 {
		// assume we have no match
		match := false
		h := md5.New()
		// build string to hash
		data := doorid + strconv.Itoa(idx)
		io.WriteString(h, data)
		// md5 checksum from string doorid + current index
		hash := h.Sum(nil)
		// create a string of hex with leading zeros
		hashst := fmt.Sprintf("%#x", hash)
		// see if first 5 characters are 0
		if hashst[:7] == "0x00000" {
			// we have a match
			match = true
		}
		// increment index here in case we don't have a match
		idx++
		if !match {
			// with no match we don't want to update the password
			continue
		}
		// ignore erroneous i and skip doubles
		if rune(hashst[7]) > 47 && rune(hashst[7]) < 56 {
			i, _ := strconv.Atoi(string(hashst[7]))
			if password[i] == "" {
				password[i] = string(hashst[8])
				ccount++
				fmt.Println(password)
			}
		} else {
			continue
		}
	}
	fmt.Printf("Time spent hashing: %v\n", time.Since(start))
}
