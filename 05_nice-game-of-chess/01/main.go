package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"strconv"
	"time"
)

func main() {
	// track time for fun
	start := time.Now()
	// the door id we need the password for
	doorid := "uqwqemis"
	var password string
	var idx int
	// loop until the password is 8 characters in length
	for len(password) < 8 {
		// assume we have no match
		match := false
		h := md5.New()
		// build string to hash
		data := doorid + strconv.Itoa(idx)
		io.WriteString(h, data)
		// md5 checksum from string doorid + current index
		hash := h.Sum(nil)
		// create a string of hex with leading zeros
		hashst := fmt.Sprintf("%#x", hash)
		// see if first 5 characters are 0
		if hashst[:7] == "0x00000" {
			// we have a match
			match = true
		}
		// increment index here in case we don't have a match
		idx++
		if !match {
			// with no match we don't want to update the password
			continue
		}
		fmt.Println(hash)
		// with a match we want to add the 6th charater to the password
		// 2 for 0x + 6 from 0-index is 7
		password += string(hashst[7])
		fmt.Println(password)
	}
	fmt.Println(password)
	fmt.Printf("Time spent hashing: %v\n", time.Since(start))
}
